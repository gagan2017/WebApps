﻿using System;
using System.IO;

namespace MyBankMVC
{
	public class ConnectingFile
	{
		string result = "";

		public void dataInsert(string s)
		{
			File.WriteAllText(Constants.FilePath, String.Empty);

			Constants.open();
			foreach (char i in s)
			{
				Constants.f.WriteByte((byte)i);
			}
			Constants.close();

		}
		public string dataRetrive()
		{
			Constants.open();

			Constants.f.Position = 0;
			for (int i = 0; i < Constants.f.Length; i++)
			{
				result = result + (char)Constants.f.ReadByte();
			}
			Constants.close();

			return result;
		}
	}
}
