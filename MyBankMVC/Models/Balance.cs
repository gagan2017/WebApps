﻿using System;
namespace MyBankMVC
{
	public class Balance
	{
		private int oldBalance,newBalance;

		public int OldBalance
		{
			get { return oldBalance; }
			set { oldBalance = (int)value; }
		}
		public int NewBalance
		{
			get { return newBalance; }
			set { newBalance = (int)value; }
		}
	}
}
