﻿using System;
using System.Collections.Generic;

namespace MyBankMVC
{
	public class BankingMvc
	{
		private int id;
		private string name;
		private string email;
		private int balance;
		private string password;


		public int Id
		{
			get { return id; }
			set { id = (int)value; }
		}

		public string Name
		{
			get { return (string)name; }
			set { name = (string)value; }
		}
		public int Balance
		{
			get { return balance; }
			set { balance = (int)value; }
		}
		public string Email
		{
			get { return email; }
			set { email = value; }
		}


		public string Password
		{
			get { return password; }
			set { password = value; }
		}
	}
	public class BankObject
	{
		public List<BankingMvc> field;
		public BankObject()
		{
			field = new List<BankingMvc>();
		}

	}
}
