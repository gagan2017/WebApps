﻿using System;
namespace MyBankMVC
{
	public enum BankName
	{
		Syndicate,
		Sbi,
		Icici,
		Other
	}
	public class LogIn
	{
		private string name;
		private string email;
		int balance;
		private string password;
		private string password1;
		public string Name
		{
			get { return (string)name; }
			set { name = (string)value; }
		}
		public int Balance
		{
			get { return balance; }
			set { balance = (int)value; }
		}
		BankName bName;
		public BankName BName
		{
			get { return bName; }
			set { bName = value; }
		}
		public string Email
		{
			get { return email; }
			set { email = value; }
		}

		public string Password
		{
			get { return password; }
			set { password = value; }
		}
		public string Password1
		{
			get { return password1; }
			set { password1 = value; }
		}
	}
}
