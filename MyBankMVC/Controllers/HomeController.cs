﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace MyBankMVC.Controllers
{
	public class HomeController : Controller
	{

		public static int editId=0;
		public static bool islogIn = false;
		public static BankObject bobj;
		public static string fp="";
		public static BankingMvc bMvcObj;
		public static JsonParsing j;
		public static ConnectingFile fs;

		public static void init(BankName b)
		{
			
			switch (b)
			{
				case BankName.Icici:Constants.FilePath =Constants.FilePathHalf+ Constants.Icici;break;
				case BankName.Syndicate: Constants.FilePath =Constants.FilePathHalf+ Constants.Syndicate; break;
				case BankName.Sbi: Constants.FilePath =Constants.FilePathHalf+ Constants.Sbi; break;
				case BankName.Other: Constants.FilePath =Constants.FilePathHalf+ Constants.Other; break;
			}

		}
		public static void init()
		{
			Constants.getFileContent();

			bobj = new BankObject();
			j = new JsonParsing();
			fs = new ConnectingFile();
			j.parse(Constants.FileContent, bobj);
		}

		public ActionResult Launch()
		{

			return View(new LogIn());
		}
		[HttpPost]
		public ActionResult Launch(LogIn b)
		{
			init(b.BName);
			return RedirectToAction("Index", "Home");
		}

		public ActionResult Index()
		{

			init();
			return View(new LogIn());
		}


		[HttpPost]
		public ActionResult Index(LogIn m)
		{


			foreach (BankingMvc b in bobj.field)
			{
				if (b.Email.Equals(m.Email) && b.Password.Equals(m.Password))
				{
					//bMvcObj = b;
					//return View("LoggedIn");
					return	RedirectToAction("LoggedIn", "Home",  b );
				}
				
			}
			return View("Failure");
		}


		[HttpGet]
		public ActionResult LoggedIn(BankingMvc arg)
		{
			bMvcObj = arg;
			return View(bMvcObj);
		}


		public ActionResult SignUp()
		{

			return View("SignUp");
		}

		[HttpPost]
		public ActionResult SignUp(LogIn m)
		{
			
			if (m.Password.Equals(m.Password1))
			{
				
				BankingMvc b = new BankingMvc();
				b.Name = m.Name;
				b.Balance = 0;
				b.Email = m.Email;
				b.Password = m.Password;

				Random rnd = new Random();
				int count = rnd.Next(1, 10000);
				b.Id = count;
				bobj.field.Add(b);
				string s = j.converToJson(bobj);
				fs.dataInsert(s);
				return RedirectToAction("Create", "Home", b);
			}

			else
			{
				return View();
			}
		}

		public ActionResult Edit(int id)
		{
			editId = id;
			foreach (BankingMvc b in bobj.field)
			{
				if (b.Id == id)
				{
					b.Balance = bMvcObj.Balance;
					return View(b);

				}
			}

			return Content("not found");
		}
		[HttpPost]
		public ActionResult Edit(BankingMvc b )
		{
			foreach (BankingMvc b1 in bobj.field)
			{
				if (b1.Id == editId)
				{
					b1.Name = b.Name;
					b1.Balance = b.Balance;
					b1.Password = b.Password;


				}
			}
			string s = j.converToJson(bobj);
			fs.dataInsert(s);

			return RedirectToAction("Create", "Home", b);
		}


		public ActionResult Delete()
		{
			return RedirectToAction("Index", "Home");
		}

		[HttpGet]
		public ActionResult Delete(int id)
		{
			foreach (BankingMvc b in bobj.field)
			{
				if (b.Id == id)
				{
					bobj.field.Remove(b);
					update();
					break;
				}
			}
			return RedirectToAction("Index", "Home");
		}


		[HttpGet]
		public ActionResult Create(BankingMvc arg)
		{

			return View(arg);
		}

		public ActionResult Credit(int id)
		{
			Balance b = new Balance();
			b.OldBalance = bMvcObj.Balance;
			return View(b);
		}
		[HttpPost]
		public ActionResult Credit(Balance obj)
		{
			bMvcObj.Balance = obj.NewBalance + bMvcObj.Balance;
			foreach (BankingMvc b1 in bobj.field)
			{
				if (b1.Id == bMvcObj.Id)
				{

					b1.Balance = bMvcObj.Balance;				}
			}
			string s = j.converToJson(bobj);
			fs.dataInsert(s);

			return RedirectToAction("LoggedIn", "Home", bMvcObj);

		}

		public ActionResult Withdraw(int id)
		{
			Balance b = new Balance();
			b.OldBalance = bMvcObj.Balance;
			return View(b);
		}
		[HttpPost]
		public ActionResult Withdraw(Balance obj)
		{
			if (obj.NewBalance > bMvcObj.Balance)
			{
				return Content("Insuffient funds");
			}
			bMvcObj.Balance =  bMvcObj.Balance-obj.NewBalance ;
			foreach (BankingMvc b1 in bobj.field)
			{
				if (b1.Id == bMvcObj.Id)
				{
					
					b1.Balance = bMvcObj.Balance;
				}
			}
			string s = j.converToJson(bobj);
			fs.dataInsert(s);

			return RedirectToAction("LoggedIn", "Home", bMvcObj);

		}
		public ActionResult CheckBalance()
		{
			
			return View(bMvcObj);
		}
		public ActionResult DisplayDetails()
		{

			return View(bMvcObj);
		}

		public static void update()
		{
			string s = j.converToJson(bobj);
			fs.dataInsert(s);

		}
	}

}
